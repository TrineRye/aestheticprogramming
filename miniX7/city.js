// images
let block0, block1, block2, block3, block4, block5;
let c, f, m, w;
// background
let stage = 0;
let x = -50;
let y = 0;
let change;
let change_min;
let x_limit = 1400;
let y_limit;
let spaceX = 100
let spaceY = 150;
// money
let money = [];
let min_money = 1;
// car
let car = [];
let min_car = 5;
// fox
let fox;
// score or loss
let lose = 0;
let coin = 0;
//text
let keyColor = 45;
// level
let level = 1


function preload() {
block0 = loadImage('block0.png');
block1 = loadImage('block1.png');
block2 = loadImage('block2.png');
block3 = loadImage('block3.png');
block4 = loadImage('block4.png');
block5 = loadImage('block5.png');
c = loadImage('car.png');
f = loadImage('fox.png');
m = loadImage('money.png');
w = loadImage('fox_win.png');
}

function setup(){
  createCanvas(windowWidth,windowHeight-5);
  background(150);
  fox = new Fox();
  y_limit = height;
}

function draw(){

if(stage == 0){

  change = floor(random(0,5));
// choosing which block to show
  if (change === 0 && x <= x_limit){
    drawblock0();

  } else if (change === 1 && x <= x_limit){
    drawblock1();

  } else if (change === 2 && x <= x_limit){
    drawblock2();

  } else if (change === 3 && x <= x_limit){
    drawblock3();
  } else if (change === 4) {

// choosing between to blocks that show less than the others
  change_min = floor(random(0,2));

  if (change_min === 0 && x <= x_limit){
    drawblock4();

  } else if (change_min === 1 && x <= x_limit){
    drawblock5();
  }
}
// statement that says if x is out of limit move to a new line
  if (x >= x_limit && y < y_limit){
    x = -50;
    y+= spaceY;
  }

  // the road
  noStroke();
  fill(150);
  rect(450,0,400,height)
  // text
  textSize(19);
  fill(keyColor, 255);
  textAlign(CENTER);
  text('PRESS the ARROW KEYS to avoid the cars and catch the coins', 500,260,300)
  fill(keyColor, 160);
  textSize(17);
  text('- press any key to start', 648,330);

} // end of stage = 0

if (stage == 1){

  noStroke();
  fill(150);
  rect(450,0,400,height)   // the road (again)

  // controls the fox
if (keyIsDown(LEFT_ARROW)) {
   fox.pos.x -= 5;
 }

 if (keyIsDown(RIGHT_ARROW)) {
   fox.pos.x += 5;
 }

 if (keyIsDown(UP_ARROW)) {
    fox.pos.y -= 5;
  }

  if (keyIsDown(DOWN_ARROW)) {
    fox.pos.y += 5;
  }

// functions conected to the obejct (money)
  showMoney();
  checkMoneyNum();
  checkMoney();
// fox
  fox.show();
// functions conected to the obejct (cars)
  checkCarNum();
  showCar();
  checkHit();
  displayScore();
  checkResult();
// level function
  levelUp();
}
// changes from start screen to game screen but the background must load first
if (keyIsPressed === true && y >= y_limit) {
   stage = 1;
 }
}

function showMoney(){
  /* This for-loop creates one coin at a time that spawn randomly on the raod (see object)*/
for (let i = 0; i < money.length; i++){ // money.length means number of coins
  money[i].show();
  }
}


function checkMoneyNum(){
if(money.length < min_money){ // if there is less that the minimum amount of coins the program adds more
  money.push(new Money()); //creating a new object
  }
}

function checkMoney(){
  for (let i = 0; i < money.length; i++) {
      let d =
        dist(fox.pos.x,fox.pos.y, money[i].pos.x,money[i].pos.y); // calculates the distance between the fox and the coin
  //if it hits add  +1 to coin and remove the coin.
      if (d < fox.size.h/2) {
        coin++;
        money.splice(i,1);
    }
  }
}


function checkCarNum(){
if(car.length < min_car){ // if there is less that the minimum amount of cars the program adds more
  car.push(new Car()); //creating a new object
  }
}

function showCar(){
  /* This shows the cars and moves the cars upwards do to the -= (see object).
  This for-loop creates more cars as long as there is less than minimum on the screen*/
for (let i = 0; i < car.length; i++){ // Car.length means number of cars
  car[i].show();
  car[i].move();
  }
}

function checkHit(){
  for (let i = 0; i < car.length; i++) {
      let d =
      dist(fox.pos.x,fox.pos.y, car[i].pos.x,car[i].pos.y-25);// calculates the distance bewteen the fox and the car

  //if it hits add +1 to lose and reomve one car.
      if (d < fox.size.h/2) {
        lose++;
        car.splice(i,1);
  //if it misses remove one car.
} else if (car[i].pos.y < 0) {
        car.splice(i,1);
      }
    }
}

function displayScore() {
    textAlign(CENTER);
    textSize(17);
 // the variable coin and lose makes it possible to add a point to the counter
   fill(keyColor, 160);
    text('You have collected ' + coin + " out of 30 coins", 650,height-50);
    text('You have been hit by ' + lose + " out of 3 cars", 650,height-30);
    text('Level ' +level, 650,40);
  }

function checkResult() {
  if (lose > 2) { // if 3 or cars hits the fox, the game is over
    fill(keyColor, 255);
    textSize(26);
    text("NO MORE LIVES...GAME OVER", 650, 300);
    noLoop();
  }
}

function levelUp(){
  if (coin === 3 ){
    min_car = 6
    level = 2;
  } else if (coin === 6){
    min_car = 7
    level = 3;
  } else if (coin === 9){
    min_car = 8
    level = 4;
  } else if (coin === 12){
    min_car = 9
    level = 5;
  } else if (coin === 15){
    min_car = 10
    level = 6;
  } else if (coin === 18){
    min_car = 11
    level = 7;
  } else if (coin === 21){
    min_car = 12
    level = 8;
  } else if (coin === 24){
    min_car = 13
    level = 9;
  } else if (coin === 27){
    min_car = 14
    level = 10;
  } else if (coin === 30){
    noStroke();
    fill(150);
    rect(450,0,400,height);  // the road (again)
    fill(keyColor, 255);
    textSize(24);
    text("YOU WON - CONGRATULATIONS!", 650, 150);
    imageMode(CENTER);
    image(w,660,350,500,400);
    noLoop();
  }
}

// functions that contains the block used for the background, which changes each time
function drawblock0(){
imageMode(CORNER);
image(block0, x, y, 100,120);
  x+= spaceX // makes the block move

}

function drawblock1(){
imageMode(CORNER);
image(block1, x, y, 100,170);
  x+= spaceX // makes the block move


}

function drawblock2(){
imageMode(CORNER);
image(block2, x, y, 100,120);
  x+= spaceX // makes the block move

}

function drawblock3(){
imageMode(CORNER);
image(block3, x, y, 100,140);
  x+= spaceX // makes the block move
}

function drawblock4(){
imageMode(CORNER);
image(block4, x, y, 100,140);
  x+= spaceX // makes the block move

}

function drawblock5(){
imageMode(CORNER);
image(block5, x, y, 100,140);
  x+= spaceX // makes the block move

}
