Click [here](https://trinerye.gitlab.io/aestheticprogramming/miniX7/index.html) to view my miniX7 program

Click [here](https://gitlab.com/TrineRye/aestheticprogramming/-/blob/main/miniX7/city.js) to view my sketch.js code

Click [here](https://gitlab.com/TrineRye/aestheticprogramming/-/blob/main/miniX7/car.js) to view my car.js code

Click [here](https://gitlab.com/TrineRye/aestheticprogramming/-/blob/main/miniX7/fox.js) to view my fox.js code

Click [here](https://gitlab.com/TrineRye/aestheticprogramming/-/blob/main/miniX7/money.js) to view my money.js code

![](game_2.mp4)

### **Which MiniX have you reworked?**

For this miniX I have chosen to rework last week’s MiniX6 as Object-Oriented Programming seemed quite abstract to me at back then, which is why the game was pretty simple with almost no storyline. Revisiting this program has given me new insights into the advantages of OOP, especially making different object through classes and getting them to interact with one another. This have given me the opportunity to expand the game with more detail, levels, and goals, which add to a broader storyline. 

### **What have you changed and why?**
The overall concept of the game hasn’t changed much, but I have added more details and objects to the game to create a proper storyline. Before this week’s alterations, the game started the moment the page finished loading which is quite inconvenient as it doesn’t give the player much time to understand the game. To fix this, I created a variable called `stage` which combined with several other commands allowed me to switch between different screens if a key is pressed. The end screen is structured a little bit different as it doesn’t involve the stage variable. Instead, it is connected to the function `levelUp` which is based on several if-else statements that determines the level and difficulty of the game as well as the number of coins one must collect to win the game. If the player wins the game, all the objects on the road will be replaced by an image of a fox sitting on a stack of coins, thereby completing the storyline. Losing the game is the same as before meaning that the player can only get hit three times before losing. To understand the use of classes I decided to make all the interactive elements objects, this includes the fox, the coins, and the cars. This made it so much easier to make changes and also the interactions between the object, I felt like, became smoother. I also added levels to the game, which makes the game harder as the player collects more coins. This is due to a string of if-else statements that increases the minimum number of cars, thereby increasing the length of the array and pushing out more cars.  

<img src="miniX7/start_screen.PNG" width="350" height="270" >

<img src="miniX7/lose_screen.PNG" width="350" height="270" >

<img src="miniX7/win_screen.PNG" width="300" height="270">

### **What have you learnt in this miniX? How did you incorporate/advance/interpret the concepts in your ReadMe/RunMe (the relation to the assigned readings)?**

This week’s discussion in class gave me some perspective in relation to the assigned readings on object-oriented programming as we discussed Jonathan Shariat and Cynthia Savard Saucier’s view on “inclusive design”, their point being that “saying who we include doesn’t make us more inclusive, it ends up excluding everyone else (Shariat & Saucier, 2017).” The objects one make will most definitely inherit our subjectivity and assumptions about the world which may lead to biased and unethical abstractions. A possible solution to this can be found in Winnie Soon and Geoff Cox’s second chapter in Aesthetic Programming: A Handbook of Software Studies (2020) in their discussion about unequal representation. They suggest avoiding “violent power structures” and discrimination by rejecting universalism as it once again enhances who is not included or represented (Soon & Cox, 2020). This idea might seem unattainable as games often revolve around rather stereotypical characters such as hero, victim, and villain with trades often seen through the “male gaze”, however, to create and unbiased game what is needed might be neutrality and the qualities it has to offer.


### **What is the relation between aesthetic programming and digital culture and how does your work demonstrate the perspective of aesthetic programming?**
Aesthetic programming allows for a new kind of critical thinking that concerns itself with the politics and aesthetics of software. As such, programming becomes a dynamic and cultural practice and a means of understanding the existing technological paradigms that constitute our lives. Through programming these techno-political issues become evident, thereby giving us the change to act upon (or against) these paradigms. This notion on political aesthetic refers back to the critical ideas of Theodor Adorno and Walter Benjamin that “enforce the concept that cultural production - which would now naturally include programming - must be seen in a social context (Soon & Cox, 2020)”. As such, aesthetic programming becomes a way to understand the world around us but also make worlds and embrace different ways to explore the “relations between writing, coding, and thinking to imagine, create and propose alternatives (Soon & Cox, 2020).” 

As programming can be seen as a cultural production with a social context, the worlds that we make inhibit elements which as earlier mentioned contains assumptions about the existing world. The classes in my game are all filled with my assumptions about each object and how they are to behave, however, I am not the one who has to face the consequences of my decisions, the players are. Luckily, I don’t think my game offends anyone as it has quite a neutral storyline, but with a different character such as a man or a woman, it could be considered biased as such a binary choice would exclude a lot of people. 

### **References**

Winnie Soon & Geoff Cox; 'Aesthetic Programming: A Handbook of Software Studies', 2020

Shariat, Jonathan, & Saucier, Cynthia Savard, (2017). Tragic Design: O'Reilly Media.

https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/class07

https://editor.p5js.org/KaitlynIshida/sketches/ODwRA1Oiv?fbclid=IwAR32KIe_XrljdlWzQiJthkflxny-3pMKns2Yv4mSWb9AP-AmgG7f1lJFgNM 

**Images**

https://www.vectorstock.com/royalty-free-vector/red-cartoon-muscle-car-top-view-vector-21974587

https://www.vectorstock.com/royalty-free-vector/cute-cartoon-fox-in-modern-simple-flat-style-vector-20261488

https://www.vecteezy.com/vector-art/465026-businessman-use-stethoscope-checking-stack-of-gold-coins?fbclid=IwAR2OYfpMxjBB7Sc1n0lJHtkbuoJ7GoO-M2G2saxudNJmMhn86oFqbf1xsmA

https://www.vectorstock.com/royalty-free-vector/money-coin-cartoon-vector-23565750 

