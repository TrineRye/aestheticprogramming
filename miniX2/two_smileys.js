// red, green, and blue color values
let r, g, b, r1, g1, b1, r2, g2, b2, r3, g3, b3;

// the angry face's coordinates
let x = 300;
let y = 350;

//used for the nervous circle (diameter)
let eye_size = 130

let iris_size = 60;

let glare_size = 20;

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(12)
  // Pick colors randomly
  r = random(250);
  g = random(250);
  b = random(250);
  r1 = random(250);
  g1 = random(250);
  b1 = random(250);
  r2 = random(250);
  g2 = random(250);
  b2 = random(250);
  r3 = random(250);
  g3 = random(250);
  b3 = random(250);
}

function draw() {
  background(30);

  push();
  let o="hold mouse over emoji"
  strokeWeight(1);
  stroke(255);
  textSize(25);
  fill(random(100,200),random(100,200),random(100,200));
  textAlign(CENTER);
  text(o,330,250,600,300);
  pop();

  push();
  let or="or"
  strokeWeight(1);
  stroke(255);
  textSize(25);
  fill(random(100,200),random(100,200),random(100,200));
  textAlign(CENTER);
  text(or,330,280,600,300);
  pop();

  push();
  let c="click to change color"
  strokeWeight(1);
  stroke(255);
  textSize(25);
  fill(random(100,200),random(100,200),random(100,200));
  textAlign(CENTER);
  text(c,330,310,600,300);
  pop();

  let h ="how do you feel at the moment?" // text
  strokeWeight(2);
  stroke(255);
  textSize(35);
  fill(random(100,200),random(100,200),random(100,200)); // makes the text change color random
  text(h,400,40,800,300);

  let a = "angry" // text
  strokeWeight(2);
  stroke(255);
  textSize(35);
  fill(random(100,200),random(100,200),random(100,200)); // makes the text change color random
  text(a,260,100,800,300);

  let n = "nervous" // text
  strokeWeight(2);
  stroke(255);
  textSize(35);
  fill(random(100,200),random(100,200),random(100,200)); // makes the text change color random
  text(n,900,100,800,300);


// **The angry emoji**
  strokeWeight(3);
  stroke(r1, g1, b1);
  fill(r1, g1, b1,150);
  ellipse(x, y, 400, 400); // face

  noStroke();
  fill(255);
  ellipse(220, 300, 130);// left eye
  ellipse(380, 300, 130);// right eye

  // left iris
  let xc =constrain(mouseX, 200, 240);
  let yc = constrain(mouseY, 280,320);
  strokeWeight(10);
  stroke(r,g,b);  // makes the eyes change color
  fill(0);
  circle(xc,yc,60);

  // right iris
  let xc1 =constrain(mouseX, 362, 402);
  let yc1 = constrain(mouseY, 280,320);
  strokeWeight(10);
  stroke(r,g,b); //makes the eyes change color
  fill(0);
  circle(xc1,yc1,60);

  noStroke();
  fill(255);
  circle(xc+20,yc-15,20); // left glare
  circle(xc1+20,yc1-15,20); // right glare

  // check to see if the mouse is withi the angry circle
  let d = dist(mouseX, mouseY, 300, 350);
  if (d < 200) {
    x = floor(random(290,310));
    y = floor(random(340,360));
    y1 = floor(random(320,325));
    y2 = floor(random(275,280));
    noStroke();
    fill(r1,g1,b1);
    angleMode(DEGREES);
    arc(220, y1, 122, 90 , 0, 180, CHORD); //lower left
    arc(380, y1, 122, 90 , 0, 180, CHORD); // lower right
    arc(220, y2, 122, 90 , 180, 0, CHORD); // upper left
    arc(380, y2, 122, 90 , 180, 0, CHORD); // loupper right
  }

//**The nervous emoji**

// check to see if the mouse is withi the nervous circle
let e = dist(mouseX, mouseY, 960,350); //
if (e < 200) {
// if so change the size og the eyes
  eye_size = floor(random(110,140));
  iris_size = floor(random(60,80));
  glare_size = floor(random(20,30));

  strokeWeight(3)
  stroke(r2,g2,b2);
  fill(0,0,0,0);
  ellipse(960,350,400,400); // face
  rect();
}
  // the nervous eyes
  noStroke();
  fill(255);
  ellipse(880, 300, eye_size);// left eye
  ellipse(1040, 300, eye_size);// right eye

  strokeWeight(10)
  stroke(r3,g3,b3);
  fill(0);
  ellipse(880, 300,iris_size);// left iris
  ellipse(1040, 300,iris_size);// right iris

  noStroke();
  fill(255);
  ellipse(880+20,300-15,glare_size);// left glare
  ellipse(1040+20,300-15,glare_size);// right glare
}

// When the user clicks the mouse
  function mousePressed() {
  // Check to se if the mouse is inside the angry circle
  let d = dist(mouseX, mouseY, 300, 350);
  if (d < 200) {
  // if so pick new random color values
    r = random(250);
    g = random(250);
    b = random(250);
    r1 = random(250);
    g1 = random(250);
    b1 = random(250);
  }
  // Check to se if the mouse is inside the nervous circle
  let e = dist(mouseX, mouseY, 960,350);
  if (e < 200) {
    // if so pick new random color values
    r2 = random(250);
    g2 = random(250);
    b2 = random(250);
    r3 = random(250);
    g3 = random(250);
    b3 = random(250);
  }
}
