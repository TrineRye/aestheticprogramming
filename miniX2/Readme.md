## MiniX2

[_Click to view my **miniX2**_](https://trinerye.gitlab.io/aestheticprogramming/miniX2/index.html)

[_Click to view my **code**_](https://gitlab.com/TrineRye/aestheticprogramming/-/blob/main/miniX2/two_smileys.js)


The video below showcases the two emojis: the angry and the nervous one.

![](two_smileys.mp4)

### **Description of my program, what I have used and learnt**

To avoid differentiating between different cultures and ethnicities, I focused solely on the emotions that my emojis display. My emojis are not an accurate representation of humans since their skin- and eye color change to a random color within the "r, g, b" color codes when `mousePressed`, but the emotions displayed, I think, are something that people can relate to. When creating the angry emoji, I started with an `ellipse()` as the face and then used the `dist()` combined with an if function to check whether or not the mouse was inside the circle. If so, the emoji will shake and squint its eyes as if angry. To create these movements, I defined variables tied to the face by using the let function and assigned it a random value within a specified range with the `floor(Random())` function. However, I wanted the emoji to exhibit even more human behavior, so I used the `constrain()` function and set the irises' x- and y-coordinates to mouseX, mouseY to make the them follow the mouse within the contrains. The code behind the eye movement is inspired by another person's code, which is the second link below.

For the nervous emoji, the face is invisible to illustrate that it hides from the world, but when touched, it will show its face and how nervous it truly is. Once again I use the `dist()` combined with the if function to check if the mouse is within range and if so, the circle will appear. Also, by declaring the eye size as a variable and using the `floor(Random())` function, the eyes change in size to show just how scared the emoji is to be seen by the world.

### **How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?**

This week’s assignment was to make two emojis that considered a broader social and cultural context since bad gender representations and racial discrimination are still a reality across many devices and operating systems. While “using emojis may be fun and expressive […] they also tend to oversimplify and universalize differences, thereby perpetuating normative ideologies within already ‘violent power structures,’ such that only selected people, those with specific skin tones for instance, are represented while others are not (Soon & Cox, 2020).” Even though skin tone modifiers may be seen as a solution in the tech-industry, people are still being excluded as not all fell represented by the limited option of tones. This unequal representation also comes to show in datasets used in technologies such as facial recognition that are based on a disproportionate number of white faces, which leads to faulty identification of people with darker skin tones. Compared to the emoji, this kind of under representation has serious consequences at it leads to discrimination of people with darker skin, not giving them the same opportunities as their “whiter” counterparts, whether it be the access to technologies or lower accuracy on facial recognition in criminal cases. Returning to the politics of representation, a solution to this issue could be rejecting universalism as it enhances who is not included or represented. Also, one must remember than an emoji harbors other qualities then physical attributes and that its original purpose was to display human emotion in image form.


### References

Winnie Soon & Geoff Cox; 'Aesthetic Programming: A Handbook of Software Studies', 2020

https://p5js.org/reference/#/p5/let

https://editor.p5js.org/edwardjmartin/sketches/XbjbWuuKG

https://p5js.org/reference/#/p5/constrain

https://p5js.org/reference/#/p5/dist

https://p5js.org/reference/#/p5/if-else

https://p5js.org/reference/#/p5.Element/mousePressed

https://p5js.org/reference/#/p5/floor






