## mRNA - a part of life

Click [here](https://trinerye.gitlab.io/aestheticprogramming/miniX5/index.html) to view my miniX5 program

Click [here](https://gitlab.com/TrineRye/aestheticprogramming/-/blob/main/miniX5/mRNA.js) to view my code

![](mRNA.mp4)

### **What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?**

In class, Winnie asked us “what randomness is” and it started a conversation about how design doesn’t leave much room to randomness but is a result of conscious decisions made by humans. However, one person mentioned how randomness could be found within some biological processes and this statement inspired me to create a generative program that illustrates the randomness that sometimes occurs when DNA gets transcribed to mRNA which might alter the code sequence and result in a mutation. 

The rules within my program are as follows:
1.	Always start with a start-codon which follows the sequence: AUG. 
2.	After the start-codon the code sequence must follow a random pattern made from a combination of the four mRNA bases. 
3.	The last triplet within the code sequence must be a stop-codon and it should follow one of the three combinations: UAA, UAG, or UGA.
4.	The bases should be divided into a collection of three to create a triplet which reads as an amino acid.

How well a program runs over a longer period of time is according to Nick Montfort et al. an important test in relation to the quality of pseudorandom number generators (Montfort et al., 2012). I have set a limit within my program so it only runs until the stop-codon is done, therefore, when the last base’s x- and y- position exceeds the stop-codon the code sequence will stop and only the text will remain dynamic. I did this as it seemed pointless to keep the program running after the stop-codon since a sudden stop-codon within an unfinished protein creates a mutation which could make the protein non-effective. The mRNA code sequence is programmed to select a random base sequence each time the program is run (with the exception of the start/end codon). According to Nick Montfort et al. when a program is used “to generate sequences of numbers that appear to be uniformly distributed (Montfort et al., 2012)” the process is then deterministic rather than random, also known as pseudorandom. My mRNA sequence is relatively short compared to most proteins, which is why the process seems random, however, if the sequence was longer, the generated sequence would start to diverge from a true random process do to the statistical properties within the p5.js random function. Even though the program doesn’t use a true random process, it makes one reflect on the true randomness when a mutation occurs somewhere in the protein synthesis, altering the sequence and possibly the functionality of said protein. 

The syntaxes that produce the emergent behavior are the `x+= spaceX` and `y+= spaceY` which moves the pattern forward and creates a new line when the sequence reaches the edge. However, they are only useful as long at the bases’ x- and y-coordinates stays within the preset limit. The same syntax is also used within the for-loop to create the five vertical lines that divides the code sequence for each row. 


### **What role do rules, and processes have in your work?**

Since the program illustrates a real biological process i.e., the translation from mRNA to amino acids, I had to consider the rules of biology when creating my own rules. Therefor, as earlier mentioned, the mRNA code sequence had to start with a start-codon and end with a stop-codon. Also, the bases within the code sequence should create a triplet which refers to an amino acid (see the table). Therefor the sequence is divided by a line after every third base. The main element within the program is the second rule which states that each new base must be chosen at random out of four possible combinations since this is what creates the illusion of randomness. The program is structured with several if or if-else statements that controls what to display in each sequence of the code, however the start/end-codon is structured differently from the middle sequence since it only used an if statement to check whether the condition holds up and not if-else. 

#### **References**

Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142 

Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.

https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/class06#4-10-print 

https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch5_AutoGenerator/ 







 
