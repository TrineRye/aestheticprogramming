let x = 20;
let y = 10;
let spaceX = 50;
let spaceY = 120;
let spaceL = 150;
let w = 25;
let h = 35;
let img;
let change;
let AU;
let CG;
let GC;
let UA;
let stop;

function preload() {
img = loadImage('table.png');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255);
  strokeWeight(3);
  frameRate(10);
}

function draw() {
imageMode(CENTER);
image(img, 940,200, 580, 370);

/* I calculated where the first and last three bases would be positioned and set x to
equal that position to determine what to draw if the statement were true */

// first rule: always start with the start codon AUG

if (x == 20 && y == 10){ // the starting x value is 20, then I add 50 (spaceX) to the current value to get the next i.e. 70 and 120.
  drawAU();
}

if (x == 70 && y == 10){
  drawUA();
}

if (x == 120 && y == 10){
  drawGC();
}

// second rule: choose a random combination of RNA bases
change = floor(random(0,4));

if (change == 0 && x < 600){ // the last base has the x-coordinate 570 so the limit is sat to 600
  drawAU();
} else if (change == 1 && x < 600){
  drawUA();
} else if (change == 2 && x < 600){
  drawCG();
} else if (change == 3 && x < 600){
  drawGC();
}

if (x > 600 && y < 490){
    x = 20;
    y+= spaceY;
  }

// third rule; always end with a stop codon
stop = floor(random(0,3));

      if (stop == 0){
        if (x == 470 && y >= 490){
          drawUA();
        }
        if (x == 520 && y >= 490){
          drawGC();
        }
        if (x == 570 && y >= 490){
          drawAU();
        }

      } else if (stop === 1){
        if (x == 470 && y >= 490){
          drawUA();
        }
        if (x == 520 && y >= 490){
          drawAU();
        }
        if (x == 570 && y >= 490){
          drawAU();
        }

      } else if (stop == 2){
        if (x == 470 && y >= 490){
          drawUA();
        }
        if (x == 520 && y >= 490){
          drawAU();
        }
        if (x == 570 && y >= 490){
          drawGC();
        }
      }

// forth rule: the line should divide the mRNA code sequence into triplets

for (let x1 = 5; x1 < 610; x1++ ){
  fill(0);
  rect(x1,y,2,75);
  x1+= spaceL;
}

push();
if (x > 600 && y >= 490){
  textAlign(CENTER);
  textSize(40);
  strokeWeight(4);
  stroke(random(100,200), random(100,200), random(100,200));
  fill(255);
  text("YOUR mRNA IS DONE! - GO AHEAD AND TRANSLATE IT",720,410,450);
}
pop();

}

function drawAU(){
  push();
  noStroke();
  fill(240,101,187); // pink / adenine
  rect(x,y,w,h,10);
  push();
  textAlign(CENTER);
  textSize(15);
  fill(0);
  text("A",x,y+12,w,h)
  pop();

  push();
  fill(36,112,180); // blue / uracil
  rect(x,y+40,w,h,10);
  push();
  textAlign(CENTER);
  textSize(15);
  fill(0);
  text("U",x,y+52,w,h)
  pop();
  x+= spaceX;
}

function drawUA(){
  push();
  noStroke();
  fill(36,112,180); // blue / uracil
  rect(x,y,w,h,10);
  push();
  textAlign(CENTER);
  textSize(15);
  fill(0);
  text("U",x,y+12,w,h)
  pop();

  push();
  fill(240,101,187); // pink / adenine
  rect(x,y+40,w,h,10);
  push();
  textAlign(CENTER);
  textSize(15);
  fill(0);
  text("A",x,y+52,w,h)
  pop();
  x+= spaceX;
}

function drawCG(){
  push();
  noStroke();
  fill(255,215,0); // yellow / cytosine
  rect(x,y,w,h,10);
  push();
  textAlign(CENTER);
  textSize(15);
  fill(0);
  text("C",x,y+12,w,h)
  pop();

  push();
  fill(56,173,5); // green / guanine
  rect(x,y+40,w,h,10);
  push();
  textAlign(CENTER);
  textSize(15);
  fill(0);
  text("G",x,y+52,w,h)
  pop();
  x+= spaceX;
}

function drawGC(){
  push();
  noStroke();
  fill(56,173,5); // green / guanine
  rect(x,y,w,h,10);
  push();
  textAlign(CENTER);
  textSize(15);
  fill(0);
  text("G",x,y+12,w,h)
  pop();

  push();
  fill(255,215,0);   // yellow / cytosine
  rect(x,y+40,w,h,10);
  push();
  textAlign(CENTER);
  textSize(15);
  fill(0);
  text("C",x,y+52,w,h)
  pop();
  x+= spaceX;
 }
