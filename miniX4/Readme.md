#### _Disclaimer_
There might accure a glitch in the beginning of the program when pressing the button “start scanning”. If so, just refresh the program and try again. 

Also, one has to refresh the page for every “scan”.


Click [here](https://trinerye.gitlab.io/aestheticprogramming/miniX4/index.html) to view my miniX4 program

Click [here](https://gitlab.com/TrineRye/aestheticprogramming/-/blob/main/miniX4/stress-o-meter.js) to view my code

![](stress-o-meter.mp4)

# **Stress-O-meter**
The “Stress-O-meter” is an antempt on a critical design pieces that adresses datafication and the relationsship between “Big data corporations” and the user. The program isn’t cabable of calculating anything but is an illusion of such an experience. 
Before starting the program, the user has to allow acess to the computers webcam in order to execute the program as intended. When acess is granted, the user is faced with a new dilemma: “to push or not to push the button”. The button’s seductivness persuates the user to press it and a as reward for doing so, the program evolves into an experience that engages with the user. The action activates a “facescanner” on the screen that “measures” the user’s facial structures until the countdown reaches 100%. When finished scanning, the user’s “stress levels” will be shown within the O-meter, but more importantly the user must give an answer on whether or not they agree with the AI’s assesment of their current stress level. Either way the program will respond with a “thank you” and show how the data is being processed and sold to other companies, illustrating the true relation between user and Big data corporations.  

### **Describe your program and what you have used and learnt**

The main elements within this program is the `createCapture(VIDEO)` function and the ctracker library, which displays the webcam `image(capture)` and allows for objects to follow the ctracker facetracker positions made by by data scientist Adum M. Øygard (2014). The program is structered with severel if-else statements that controls at which point certain elements appear on screen as well as when to start the program and change mode; example _if(mode === 1){ _start program_}_ or _if(countS < 100){_do something_}_. The `countS` is a variable which I have declared to keep track of the countdown, so in a way, this is the program internal clock that calculates at which point to add or remove different elements. I used a facetracker related syntax _positions[33][0],positions[33][1]+y_ to make the scanner follow the face and added the variable y to add movment and control the speed (the same goes for the variable x). Most of the graphical elements are images, boxes and text, however, a new element that I used this week was the createButton syntax to add user interactivity to the program and also some CSS syntax to style the buttons. Lastly, to run the program without the functions interfereing with each other, I had to use `push() and pop()` for almost every new element that I added to the program.
This week I had a lot of problems with my code on how to structure it so things would appear on the screen at the right time. To understand how to do so, I looked at _Christian Holm’s_ code https://gitlab.com/ChristianRohde/p5/-/blob/master/miniX4/sketch.js, which is where I got the inspiration to make a scanner. 


### **Articulate how your program and thinking address the theme of “capture all” and what are the cultural implications of data capture?**

As Mejias, Ulises A. and Nick Couldry mentions in the article Datafication “the term ‘datafication’ implies that something is made into data.” In relation to Human-Computer interaction the object that corporations seek to render as data is human behavior, thereby making life susceptible to being processed, analyzed, and automated on a large scale. José van Dijck interprets datafication as “a means to access [...] and monitor people’s behavior” and to me, I see this as a paradox since easy access to user knowledge can lead to progress and the development of new technologies, however, the word “monitor” implies an acceptance of “surveillance capitalism” which seems invasive in terms of user autonomy (Mejias & Couldry 2019).

The Stress-O-meter addresses datafication in that it highlights the possibilities within HCI in terms of rendering human experience into data. What I think makes this program great, is its ability to illustrate the relationship between user and “service provider” and how engaging with a program often leads to automated acceptance of terms and conditions, which permits usage and sale of one’s data. 

### **Reference** 

Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119

Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021. https://policyreview.info/concepts/datafication

https://p5js.org/reference/#group-DOM

**Picture**
https://www.monsterindia.com/career-advice/how-well-your-current-job-scores-on-the-love-o-meter/

