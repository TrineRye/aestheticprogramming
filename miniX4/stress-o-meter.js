let ctracker;
let capture;
let button;
let buttonYes;
let buttonNo;
let countS = 0;
let mode = 0;
let space = -100
let speed = 12;
let x = 730
let rando;

function preload() {
meter = loadImage('meter.png');
file = loadImage('file.png'); // uploades the image
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(25);

  rando = int(random(0,101)); // draws a random number from 0-100

  push();
  button = createButton('Start scanning');
  button.style("color","#FFFFFF"); //e.g button.style("color", "#fff");
  button.style("background", "#232E55");
  button.size(150,30);
  button.position(240,500);
  button.mousePressed(changeScan);
  pop();

  //web cam capture
 capture = createCapture(VIDEO);
 capture.size(640, 480);
 capture.hide();

 //setup face tracker
 ctracker = new clm.tracker();
 ctracker.init(pModel);
 ctracker.start(capture.elt);
}

function draw() {
  background(255);
  //draw the captured video on a screen with the image filter
  image(capture, 0,0, 640, 480);

  if(mode == 1){
    // count down
    textAlign(CENTER);
    fill(0,255,0);
    textSize(30);
    text(countS + '%', 320,50);

    if (countS < 100){
      countS ++;
      // makes the scanner run until countS reaches 100%
    push();
    fill(0,255,0);
    textAlign(CENTER);
    textSize(15);
    text('Measuring Stress Levels',320,20);
    pop();
    // the scanner
    let positions = ctracker.getCurrentPosition();
    noStroke();
    fill(255,180);
    rectMode(CENTER);
    rect(positions[33][0],positions[33][1]+space,200,15,20); // by adding y the face tracker position starts higher than position[33][1]
    space = space + speed; // makes the rect move

    if (space < -100 || space > 140) { // -100 is the forehead limit and 140 is the chin limit
    speed = speed * -1;
    }
  }
}
  if (countS == 100){
    push();
    noStroke();
    fill(35,46,85);
    rectMode(CENTER);
    rect(820,150,300,260,20); // blue box
    pop();
    push();
    textAlign(CENTER);
    stroke(255);
    fill(random(100,200),random(100,200),random(100,200));
    textSize(30);
    text("Do you agree with the AI's assesment?", 820,50,250);
    textSize(25);
    text("Current Stress Level", 505, 240,200);
    noStroke();
    fill(0,255,0);
    textSize(20);
    text('COMPLETE',320,20);
    pop();

    buttonYes = createButton('YES'); // yes botton
    buttonYes.style("color","#232E55");
    buttonYes.style("background", "#FFFFFF");
    buttonYes.size(60,40);
    buttonYes.position(730,200);
    buttonYes.mousePressed(changeNext);

    buttonNo = createButton('NO'); // no botton
    buttonNo.style("color","#232E55");
    buttonNo.style("background", "#FFFFFF");
    buttonNo.size(60,40);
    buttonNo.position(860,200);
    buttonNo.mousePressed(changeNext);

    push();
    imageMode(CENTER);
    image(meter, 550,335, 550, 420); // meter image
    stroke(255);
    fill(0);
    textAlign(CENTER);
    textSize(45);
    text(rando+"%",515,425); // random stress level number
    pop();
  }


if (mode === 2){

  textAlign(CENTER); // green 100% text
  fill(0,255,0);
  textSize(30);
  text(countS + '%', 320,50);

  push();
  stroke(255);
  fill(35,46,85);
  textAlign(CENTER);
  textSize(30);
  text("Thank you for helping us train our AI", 820,300,250);
  textSize(15);
  text("selling data...", 815,450);
  imageMode(CENTER)
  image(file, x, 450, 80, 60); // moving file
  image(file, 900,450, 80, 60);// right file
  x = x + 2; // makes the file.image move

    if (x > 900){ // makes the file.image move in a loop
      x = 730
    }
  pop();
  }
}

function changeScan() {
   mode = 1 // makes the program start
}

function changeNext() {
  mode = 2 // starts an action when pressing the button
}
