let screen = 0; //introducScreen (city sign)
//Arrays for preloading of quiz images
let act = [];
let ceo = [];
let cor = [];
let int = [];
let jan = [];
let rec = [];
let tec = [];
let uxd = [];
//Array for preloadiing of ID cards
let id = [];
//Array for moving cloud objects (from class)
let clouds = [];
//Arrays for preloading of images of buildings inside and outside
let buildings = [];
let inside = [];
//Arrays for eachs profile (empty from start)
let actValue = [];
let ceoValue = [];
let corValue = [];
let intValue = [];
let janValue = [];
let recValue = [];
let tecValue = [];
let uxdValue = [];
//GLobal variables (incremeants later in the program)
let num = 0;
let clicks = 0;
let time = 0;
//JSON-file with questions for quiz
let questions;
//Variable to define access level (according to profile)
let access;
//Buttons to changes images in quiz, reload (error), reload (end)
let button0, button1, button2, button3, button4, button5, button6, button7, buttonReload, buttonEnd;


function preload(){ //Preload images, JSON-files and font
  sky = loadImage("other_images/background.png");
  white_photos = loadImage("other_images/white_photos.png");
  error_message = loadImage("other_images/error_message.png")
  landscape = loadImage("other_images/landscape.png")
  end_message = loadImage('other_images/end_message.png')
  questions = loadJSON("questions.json");
  buildings = loadJSON("buildings.json");
  font = loadFont('excluded_font.ttf');
  //For-loops to preload images for quiz (divided in profiles)
  for (let i = 0; i < 9; i++){
  act[i] = loadImage("new_quiz_images/act"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  ceo[i] = loadImage("new_quiz_images/ceo"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  cor[i] = loadImage("new_quiz_images/cor"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  int[i] = loadImage("new_quiz_images/int"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  jan[i] = loadImage("new_quiz_images/jan"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  rec[i] = loadImage("new_quiz_images/rec"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  tec[i] = loadImage("new_quiz_images/tec"+i+".png")
  }

  for (let i = 0; i < 9; i++){
  uxd[i] = loadImage("new_quiz_images/uxd"+i+".png")
  }
  //For-loops to preload ID cards for each profiles)
  for (let i = 0; i < 8; i++){
  id[i] = loadImage("ID/id"+i+".png")
  }
  //For-loops to preload images for buildings outside
  for (let i = 0; i < 11; i++){
  buildings[i] = loadImage("building_images/building"+i+".png")
  }
  //For-loops to preload images for buildings inside
  for (let i = 0; i < 15; i++){
  inside[i] = loadImage("inside_building_images/inside"+i+".png")
  }
}

function setup (){ //Creating Canvas
  createCanvas(windowWidth, windowHeight);
  //Create buttons with images for 1st quiz round
  button0 = createImg("new_quiz_images/act0.png", "error");
  button1 = createImg("new_quiz_images/ceo0.png", "error");
  button2 = createImg("new_quiz_images/cor0.png", "error");
  button3 = createImg("new_quiz_images/int0.png", "error");
  button4 = createImg("new_quiz_images/jan0.png", "error");
  button5 = createImg("new_quiz_images/rec0.png", "error");
  button6 = createImg("new_quiz_images/tec0.png", "error");
  button7 = createImg("new_quiz_images/uxd0.png", "error");
  //Hide buttons again (done to make hiding of buttons possible later)
  button0.hide();
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
  button5.hide();
  button6.hide();
  button7.hide();
  }
//Function to display background and static clouds
function backdrop(){
  background(sky);
  showClouds();
}
//Function to manage number of clouds
function checkCloudsNum(){
if(clouds.length < 4){ //If there is less that the minimum amount of clouds on the screen the program adds more
  clouds.push(new Clouds()); //Creating a new cloud object
  }
}
//Function to check if clouds have left the screen
function outClouds(){
  for (let i = 0; i < clouds.length; i++) {//For-loop to check all (four) clouds
    if (clouds[i].pos.x > width) {
      clouds.splice(i,1); //Remove cloud object
    }
  }
}
//Function to show the clouds on the screen using a for-loop
function showClouds(){
  for (let i = 0; i < clouds.length; i++){
    clouds[i].show();
  }
}
//Function to move clouds on screen using a for-loop
function moveClouds(){
  for (let i = 0; i < clouds.length; i++){
    clouds[i].move();
  }
}


function draw(){

  if (screen == 0){ //If the screen is equal to 0 show the introductionScreen (city sign)
    backdrop();
    moveClouds();
    checkCloudsNum();
    outClouds();
    startScreen();
  } else if (screen == 1){ //If the screen is equal to 1 show the infoScreen (introduction to game)
    backdrop();
    moveClouds();
    checkCloudsNum();
    outClouds();
    infoScreen();
  } else if (screen == 2){ //If screen is equal to 2 show the testScreen (quiz)
    //Clouds freezes in their current positions
    backdrop()
    testScreen();
  } else if (screen == 3){ //If screen is equal to 3 show resultScreen (ID card)
    backdrop()
    resultScreen();
  } else if (screen == 4){ //If screen is equal to 4 show mapScreen (city view)
    backdrop();
    mapScreen();
  } else if (screen == 5){ //If screen is equal to 5 show endMessage (end of game)
    backdrop();
    endMessage();
  }
}
//If the mouse is pressed, change screen value dependent on current value
function mousePressed(){
  if(screen == 0){
    screen = 1;
  } else if (screen == 1){
    screen = 2;
  } else if (screen == 3){
    screen = 4;
  }
}

function startScreen (){ //Using geometric shapes to create city sign
  //Pole
  push();
  rectMode(CORNER);
  strokeWeight(3);
  stroke(150);
  fill(160)
  rect(width/2, height/2, 30, height);
  pop();
  //Pole highlight
  push();
  noStroke();
  fill(170)
  rect(width/2+5, height/2, 10, height);
  pop();
  //Sign
  push();
  fill(34,139,34)
  rectMode(CENTER);
  strokeWeight(6);
  stroke(255)
  rect(width/2, height/3,600,250,20);
  fill(255);
  pop();
  //Text
  push();
  textAlign(CENTER);
  textFont(font)
  fill(255);
  textSize(35);
  text('welcome  to', width/2,height/4);
  textSize(38);
  text('access  is  not  ( a )  given', width/2,height/3+15);
  textSize(30);
  text('"A  game  for  thought"', width/2,height/3+50);
  fill(196,244,250)
  textSize(25);
  text('press the mouse to start', width-250, height-50);
  pop();
}

function infoScreen(){ //Designing the infoScreen
  push();
  textAlign(CENTER);
  rectMode(CENTER);
  textFont(font) //Using preloaded font
  fill(50);
  textSize(30);
  text('Before  you  play  the  game  we  want  to  get  to  know  you  better.', width/2,height/2-22, width-200, 200);
  text('In  the  following  segment,  choose  an  image  that  represents  you  the  most', width/2,height/2+100, width-200, 200);
  fill(196,244,250)
  textSize(25);
  text('press the mouse to move on', width-270, height-50);
  pop();
}

function testScreen(){ //Designing the quizScreen
  if (clicks < 9){ //Nine quiz rounds and questions
    fill(50);
    textAlign(CENTER);
    textFont(font)
    textSize(40)
    text(questions.questions[num],width/2,80) /*Calling text from JSON-file.
    Num is connected to quiz images */
  }
  //Display white frames for quiz images
  push();
  imageMode(CENTER);
  image(white_photos, width/2, height/2+50, 900,480);
  pop();
  //Begin every quiz round with removing previous buttons (no effect in first round)
  button0.hide();
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
  button5.hide();
  button6.hide();
  button7.hide();
  /*Create all eight image-buttons which changes every quiz round.
  num is a variable that connects the images with the corresponding question.
  num incremeants with each click*/
  //Button0 represents the Activist
  button0 = createImg("new_quiz_images/act"+num+".png", "error");
  button0.position(width/2-437, height/2-176);
  button0.style("width","185px");
  button0.mousePressed(printAct);
  //Button0 represents the CEO
  button1 = createImg("new_quiz_images/ceo"+num+".png", "error");
  button1.position(width/2-207, height/2-176);
  button1.style("width","185px");
  button1.mousePressed(printCeo);
  //Button0 represents the Corporate worker
  button2 = createImg("new_quiz_images/cor"+num+".png", "error");
  button2.position(width/2-437, height/2+73);
  button2.style("width","185px");
  button2.mousePressed(printCor);
  //Button0 represents the Intern
  button3 = createImg("new_quiz_images/int"+num+".png", "error");
  button3.position(width/2-207, height/2+73);
  button3.style("width","185px");
  button3.mousePressed(printInt);
  //Button0 represents the Janitor
  button4 = createImg("new_quiz_images/jan"+num+".png", "error");
  button4.position(width/2+23, height/2-176);
  button4.style("width","185px");
  button4.mousePressed(printJan);
  //Button0 represents the Receptionist
  button5 = createImg("new_quiz_images/rec"+num+".png", "error");
  button5.position(width/2+252, height/2-176);
  button5.style("width","185px");
  button5.mousePressed(printRec);
  //Button0 represents the Tech developer
  button6 = createImg("new_quiz_images/tec"+num+".png", "error");
  button6.position(width/2+23, height/2+73);
  button6.style("width","185px");
  button6.mousePressed(printTec);
  //Button0 represents the UX designer
  button7 = createImg("new_quiz_images/uxd"+num+".png", "error");
  button7.position(width/2+252, height/2+73);
  button7.style("width","185px");
  button7.mousePressed(printUxd);
  //Hide all bottons after last quiz image has been clicked
  if (clicks > 8){
    button0.hide();
    button1.hide();
    button2.hide();
    button3.hide();
    button4.hide();
    button5.hide();
    button6.hide();
    button7.hide();
    screen = 3 //Change to resultScreen
    }
}
//When a button0 is pressed, this function is called to count how many times it has been clicked
function printAct() {
  clicks++ //Incremeant global variable 'clicks'
  if (clicks < 10){ /*10 is the limit because the first string is pushed
    into an array at clicks 1 (not 0)*/
    actValue.push('act'); //Push the string 'act' in array called 'actValue'
  }
  if (num < 8){ //num ends at 8 (end of quiz)
    num++ /*When num is incremeanted, the button0 changes to new image.
    New question is displayed*/
  }
console.log(actValue);
}
//When button1 is pressed, this function is called
function printCeo() {
  clicks++
  if (clicks < 10){
    ceoValue.push('ceo');
  }
  if (num < 8){
    num++
  }
//  console.log(ceoValue);
}
//When button2 is pressed, this function is called
function printCor() {
  clicks++
  if (clicks < 10){
    corValue.push('cor');
  }
  if (num < 8){
    num++
  }
//  console.log(corValue);
}
//When button3 is pressed, this function is called
function printInt() {
  clicks++
  if (clicks < 10){
    intValue.push('int');
  }
  if (num < 8){
    num++
  }
//  console.log(intValue);
}
//When button4 is pressed, this function is called
function printJan() {
  clicks++
  if (clicks < 10){
    janValue.push('jan');
  }
  if (num < 8){
    num++
  }
//  console.log(janValue);
}
//When button5 is pressed, this function is called
function printRec() {
  clicks++
  if (clicks < 10){
    recValue.push('rec');
  }
  if (num < 8){
    num++
  }
//  console.log(recValue);
}
//When button6 is pressed, this function is called
function printTec() {
  clicks++
  if (clicks < 10){
    tecValue.push('tec');
  }
  if (num < 8){
    num++
  }
//  console.log(tecValue);
}
//When button7 is pressed, this function is called
function printUxd() {
  clicks++
  if (clicks < 10){
    uxdValue.push('uxd');
  }
  if (num < 8){
    num++
  }
//  console.log(uxdValue);
}

function resultScreen(){ //Designing the resultScreen
  backdrop();
  fill(50);
  textAlign(CENTER);
  textFont(font)
  textSize(50)
  text('your  profile  is',width/2,100)
  fill(196,244,250)
  textSize(25);
  text('press the mouse to move on', width-270, height-50);

  /*Conditinal statement to check which array is the longest.
  Goes through all arrays until one statement is true.
  If non is true, an error function is called*/
    if (actValue.length > ceoValue.length && actValue.length > corValue.length && actValue.length > intValue.length &&
        actValue.length > janValue.length && actValue.length > recValue.length && actValue.length > tecValue.length &&
        actValue.length > uxdValue.length){
        activist(); //Calls activist function, is this array is longer than all other arrays
    } else if (ceoValue.length > actValue.length && ceoValue.length > corValue.length && ceoValue.length > intValue.length &&
        ceoValue.length > janValue.length && ceoValue.length > recValue.length && ceoValue.length > tecValue.length &&
        ceoValue.length > uxdValue.length){
        CEO(); //Calls CEO function, is this array is longer than all other arrays
    } else if (corValue.length > actValue.length && corValue.length > ceoValue.length && corValue.length > intValue.length &&
        corValue.length > janValue.length && corValue.length > recValue.length && corValue.length > tecValue.length &&
        corValue.length > uxdValue.length){
        corporate(); //Calls corporate function, is this array is longer than all other arrays
    } else if (intValue.length > actValue.length && intValue.length > ceoValue.length && intValue.length > corValue.length &&
        intValue.length > janValue.length && intValue.length > recValue.length && intValue.length > tecValue.length &&
        intValue.length > uxdValue.length){
        intern(); //Calls intern function, is this array is longer than all other arrays
    } else if (janValue.length > actValue.length && janValue.length > ceoValue.length && janValue.length > corValue.length &&
        janValue.length > intValue.length && janValue.length > recValue.length && janValue.length > tecValue.length &&
        janValue.length > uxdValue.length){
        janitor(); //Calls janitor function, is this array is longer than all other arrays
    } else if (recValue.length > actValue.length && recValue.length > ceoValue.length && recValue.length > corValue.length &&
        recValue.length > intValue.length && recValue.length > janValue.length && recValue.length > tecValue.length &&
        recValue.length > uxdValue.length){
        receptionist(); //Calls receptionist function, is this array is longer than all other arrays
    } else if (tecValue.length > actValue.length && tecValue.length > ceoValue.length && tecValue.length > corValue.length &&
        tecValue.length > intValue.length && tecValue.length > janValue.length && tecValue.length > recValue.length &&
        tecValue.length > uxdValue.length){
        tech_developer(); //Calls tech_developer function, is this array is longer than all other arrays
    } else if (uxdValue.length > actValue.length && uxdValue.length > ceoValue.length && uxdValue.length > corValue.length &&
        uxdValue.length > intValue.length && uxdValue.length > janValue.length && uxdValue.length > recValue.length &&
        uxdValue.length > tecValue.length){
        ux_designer(); //Calls ux_designer function, is this array is longer than all other arrays
    } else {
        error(); //Error happens, if the (two or more) longest arrays are equal
    }
}
//These functions could have been made as classes, using info from the ID cards
function activist(){
  push();
  imageMode(CENTER);
  access = 0; /*Access level defines which messages and inside_building_images
  are displayed in the mapScreen*/
  image(id[access], width/2, height/2, 540, 330); //ID card displayed in resultScreen and mapScreen
  pop();
}

function CEO(){
  push();
  imageMode(CENTER);
  access = 1;
  image(id[access], width/2, height/2, 540, 330);
  pop();

}

function corporate(){
  push();
  imageMode(CENTER);
  access = 2;
  image(id[access], width/2, height/2, 540, 330);
  pop();
}

function intern(){
  push();
  imageMode(CENTER);
  access = 3;
  image(id[access], width/2, height/2, 540, 330);
  pop();
}

function janitor(){
  push();
  imageMode(CENTER);
  access = 4;
  image(id[access], width/2, height/2, 540, 330);
  pop();
}

function receptionist(){
  push();
  imageMode(CENTER);
  access = 5;
  image(id[access], width/2, height/2, 540, 330);
  pop();
}

function tech_developer(){
  push();
  imageMode(CENTER);
  access = 6;
  image(id[access], width/2, height/2, 540, 330);
  pop();
}

function ux_designer(){
  push();
  imageMode(CENTER);
  access = 7;
  image(id[access], width/2, height/2, 540, 330);
  pop();
}

function mapScreen(){ //Screen with city view
  // landscape
  push();
  imageMode(LEFT, BOTTOM);
  image(landscape, 0, height-400, width,400);
  pop();
  textAlign(CENTER);
  textFont(font);
  fill(196,244,250)
  textSize(25);
  text('access  is  not  ( a )  given', width/2, 50); //Title of game

  // Display buildings (preloaded in array)
  push();
  imageMode(CENTER);
  image(buildings[8], width/2+331, height-390, 220, 140); // Mansion
  image(buildings[0], width/2+160, height-318, 140, 300); // Bank
  image(buildings[9], width/2-10, height-343, 200, 350); // Office
  image(buildings[5], width/2-520, height-228, 200, 120); // Cyber Cafe
  image(buildings[3], width/2-330, height-233, 150, 130); // Bistro
  image(buildings[7], width/2-300, height-345, 100, 65); // House
  image(buildings[6], width/2-390, height-335, 80, 45); // Garage
  image(buildings[1], width/2-180, height-282, 130, 230); // Appartment
  image(buildings[4], width/2+345, height-218, 135, 100); // Coffeshop
  image(buildings[2], width/2+250, height-203, 35, 70); // ATM
  image(buildings[10], width/2+520, height-248, 190, 160); // Restaurant
  // ID card based on access number
  image(id[access], 160, 80, 240, 140);
  pop();
  //Function to display text in textboxes to each building based on access level
  appartmentTextbox();
  bankTextbox();
  bistroTextbox();
  coffeeshopTextbox();
  garageTextbox();
  houseTextbox();
  mansionTextbox();
  officeTextbox();
  restaurantTextbox();
  underground_net_cafeTextbox();
  //Frame counter that defines when endScreen appears
  time++
  if (time == 3400) {
    screen = 5
  }
}

function appartmentTextbox(){
  //If mouse in within distance of building, inside image pops up according to access level
  let d = dist(mouseX, mouseY, width/2-180, height-282);
  if (d < 65) {
    if (access == 2){ //Corporate
      push();
      imageMode(CENTER);
      image(inside[8], width/2-180, height-282, 350,200)
      pop();
    } else if (access == 3){ //Intern
      push();
      imageMode(CENTER);
      image(inside[9], width/2-180, height-282, 350,200)
      pop();
    } else if (access == 7){ //UX designer
      push();
      imageMode(CENTER);
      image(inside[14], width/2-180, height-282, 350,200)
      pop();
    } else { //Access denied
      push();
      imageMode(CENTER);
      image(inside[0], width/2-180, height-282, 350,200)
      pop();
    }
      push();
      fill(50);
      rectMode(CENTER);
      textFont('Helvetica')
      textSize(13)
      //Access refers to profile and defines the message that is displayed from the JSON-file
      text(buildings.appartment[access], width/2-180, height-195, 340, 40)
      pop();
  }
}
//Every profile has access to the bank
function bankTextbox(){
  let d = dist(mouseX, mouseY, width/2+160, height-318);
  if (d < 70) {
    push();
    imageMode(CENTER);
    image(inside[1], width/2+160, height-253, 350,200)
    pop();
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    //Message changes according to acces level aka. profile
    text(buildings.bank[access], width/2+160, height-168, 340, 40)
  }
}


function bistroTextbox(){
  let d = dist(mouseX, mouseY, width/2-330, height-233);
  if (d < 75) {
    if (access == 4 || access == 5 || access == 6){
      push();
      imageMode(CENTER);
      image(inside[2], width/2-330, height-233, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2-330, height-233, 350,200)
      pop();
    }
      fill(50);
      rectMode(CENTER);
      textFont('Helvetica')
      textSize(13)
      text(buildings.bistro[access], width/2-330, height-148, 340, 40);
  }
}

function coffeeshopTextbox(){
  let d = dist(mouseX, mouseY, width/2+345, height-218);
  if (d < 67) {
    if (access == 0 || access == 2 || access == 3 || access == 5 || access == 7){
      push();
      imageMode(CENTER);
      image(inside[3], width/2+345, height-218, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2+345, height-218, 350,200)
      pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.coffeeshop[access], width/2+345, height-133, 340, 40);
  }
}

function garageTextbox(){
  let d = dist(mouseX, mouseY, width/2-390, height-335);
  if (d < 22) {
    if (access == 0){
      push();
      imageMode(CENTER);
      image(inside[6], width/2-390, height-335, 350,200)
      pop();
    } else if (access == 6){
      push();
      imageMode(CENTER);
      image(inside[5], width/2-390, height-335, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2-390, height-335, 350,200)
      pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.garage[access], width/2-390, height-250, 340, 40);
  }
}

function houseTextbox(){
  let d = dist(mouseX, mouseY, width/2-300, height-345);
  if (d < 32) {
    if (access == 4){
      push();
      imageMode(CENTER);
      image(inside[10], width/2-300, height-345, 350,200)
      pop();
    } else if (access == 5){
      push();
      imageMode(CENTER);
      image(inside[11], width/2-300, height-345, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2-300, height-345, 350,200)
      pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.house[access], width/2-300, height-260, 340, 40);
  }
}

function mansionTextbox(){
  let d = dist(mouseX, mouseY, width/2+331, height-390);
  if (d < 70) {
    if (access == 1){
      push();
      imageMode(CENTER);
      image(inside[7], width/2+331, height-390, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2+331, height-390, 350,200)
      pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.mansion[access],width/2+331, height-305, 340, 40);
  }
}

function officeTextbox(){
  let d = dist(mouseX, mouseY,  width/2-10, height-358);
  if (d < 100) {
    if (access == 0){
      push();
      imageMode(CENTER);
      image(inside[0], width/2-10, height-358, 350,200)
      pop();
    } else {
    push();
    imageMode(CENTER);
    image(inside[13], width/2-10, height-358, 350,200);
    pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.office[access], width/2-10, height-273, 340, 40);
  }
}

function restaurantTextbox(){
  let d = dist(mouseX, mouseY, width/2+520, height-248);
  if (d < 80) {
    if (access == 1 || access == 2 || access == 6 || access == 7){
      push();
      imageMode(CENTER);
      image(inside[4], width/2+520, height-248, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2+520, height-248, 350,200)
      pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.restaurant[access], width/2+520, height-163, 340, 40);
  }
}

function underground_net_cafeTextbox(){
  let d = dist(mouseX, mouseY, width/2-520, height-228);
  if (d < 60) {
    if (access == 0 || access == 3 || access == 6 || access == 7){
      push();
      imageMode(CENTER);
      image(inside[12], width/2-520, height-228, 350,200)
      pop();
    } else {
      push();
      imageMode(CENTER);
      image(inside[0], width/2-520, height-228, 350,200)
      pop();
    }
    fill(50);
    rectMode(CENTER);
    textFont('Helvetica')
    textSize(13)
    text(buildings.underground_net_cafe[access], width/2-520, height-143, 340, 40);
  }
}

function error(){
  background(sky);
  //All quiz images are spawned quickly at random positions
  for (let i = 0; i < 9; i++){
    image(act[i], random(0,width), random(0, height), 200, 200);
    image(ceo[i], random(0,width), random(0, height), 200, 200);
    image(cor[i], random(0,width), random(0, height), 200, 200);
    image(int[i], random(0,width), random(0, height), 200, 200);
    image(jan[i], random(0,width), random(0, height), 200, 200);
    image(rec[i], random(0,width), random(0, height), 200, 200);
    image(tec[i], random(0,width), random(0, height), 200, 200);
    image(uxd[i], random(0,width), random(0, height), 200, 200);
  }
  // Display error message
  push();
  imageMode(CENTER);
  image(error_message, width/2, height/2, 600, 350);
  pop();
  // Option to press reload button
  buttonReload = createButton("RELOAD");
  buttonReload.position(width/2+160, height/2+105);
  buttonReload.mousePressed(reload); //Refers to function reload
}

function endMessage(){
  imageMode(CENTER);
  image(end_message, width/2, height/2, 700,600);
  buttonEnd = createButton("RELOAD"); //Button to reload game
  buttonEnd.size(80,30);
  buttonEnd.position(width/2+170, height/2+220);
  buttonEnd.mousePressed(reload);
  textAlign(CENTER);
  textFont(font);
  fill(196, 244, 250);
  textSize(15);
  text("This program contains images from Unsplash.com and icons from Canva.com. We do not claim ownership of these", width/2, height/30);
}

function reload() { //Function used for the button, to make the program reload, if it is pushed
  location.reload();
}
