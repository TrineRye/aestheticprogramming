Click [here](https://trinerye.gitlab.io/aestheticprogramming/final_project/index.html) to view group 4's final program

Click [here](https://gitlab.com/TrineRye/aestheticprogramming/-/tree/main/final_project) to view our repository

Click [here](https://gitlab.com/TrineRye/aestheticprogramming/-/blob/main/final_project/Group_4_Final_Readme-kopi.pdf?ref_type=heads) to view our readme about the project
