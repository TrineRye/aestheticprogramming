let map, foot, foot_right, foot_down, cat_left, stair, banner, banner1, trap_door_closed, trap_door_open, sound, steps, spell, font; // images, sound and font
let footprint, tower, name; // classes
let textbox;
let submitButton;
let speed = 50;
let spell_speed = 50;
let angle = 0;
let cat_left_x = 400;
let cat_up_y = 300;
let count = 0;
let count_1 = 0;
let count_2 = 0;
let count_3 = 0;
let count_4 = 0;
let name_data = [];
let change_name = -1;
let line_far = 1200;
let line_close = 135;
let line_up = 60;
let line_down = 500;
let wall_x = 375;
let foot_down_y = 75;
let foot_right_x = 800;
let screen = 0;
let spell_y = 350;

function preload() {
  sound = loadSound("harry_potter_theme.mp3")
  map = loadImage("map.png");
  foot = loadImage("footprint_8.png")
  foot_down = loadImage("footprint_9.png")
  foot_right = loadImage("footprint_10.png")
  cat_left = loadImage("catpaws_7.png")
  cat_up = loadImage("catpaws_5.png")
  font = loadFont('harrypotter_font.ttf');
  stair = loadImage("stair1.png");
  steps = loadImage("steps_1.png");
  banner = loadImage("banner.png");
  banner1 = loadImage("banner1.png");
  spell = loadImage("spell.png");
  trap_door_closed = loadImage("trap_door_closed.png");
  trap_door_open = loadImage("trap_door_open_3.png");

}

function setup() {
  createCanvas(windowWidth-5, windowHeight-5);
  userStartAudio();
  sound.play();
  background(map)
  textFont(font);
  angleMode(DEGREES);
  frameRate(14)

  push();
  noStroke();
  fill(105, 46, 14)
  textAlign(CENTER);
  textSize(40);
  text("''I solemnly swear that I am up to no good''", width/2, height/2)
  textSize(20);
  text("press the mouse to go to the map", width/2, height-80)

  pop();

  footprint = new Footprint();
  tower = new Tower();
  name = new Name();

  textbox = createInput('');
  textbox.position(width/2-100, 540);
  textbox.size(200)
  textbox.hide();

  submitButton = createButton('SUBMIT');
  submitButton.style('color', 'white')
  submitButton.style('background', "#760808")
  submitButton.style('border', 'none')
  submitButton.style('text-align', 'center');
  submitButton.style('font-size', '10px');
  submitButton.position(width/2+118, 539);
  submitButton.size(50,20);
  submitButton.mousePressed(textbanner);
  submitButton.hide();
}

function draw() {

  if (screen == 1){
  push();
  tint(255, 50); // Display at half opacity
  background(map);
  pop();
  footprint.show();
  tower.show();
  name.show();
  textbox.show();
  submitButton.show();
  push();
  textAlign(CENTER);
  noStroke();
  fill(105, 46, 14);
  textSize(15);
  text("Write your name and press submit to enter Hogwarts", width/2, 580)
  pop();

  if (keyIsDown(LEFT_ARROW)) {
    if (footprint.x != 400){ // right wall footprint coordinate
    footprint.x -= speed;
  } else if (footprint.y > 240 && footprint.y < 350)
    footprint.x -= speed;

   } else if (keyIsDown(RIGHT_ARROW)) {
     if (footprint.x != 350){ // left wall footprint coordinate
       footprint.x += speed
   } else if (footprint.y > 240 && footprint.y < 350)
     footprint.x += speed;

   } else if (keyIsDown(UP_ARROW)) {
     footprint.y -= speed;

   } else if (keyIsDown(DOWN_ARROW)) {
     footprint.y += speed;
   }

// tower kollision
  if ( footprint.x <= 1150 && footprint.x > 1050 && footprint.y < 350){
     footprint.x -= speed;
     push();
     textAlign(CENTER);
     textSize(30);
     noStroke();
      fill(105, 46, 14, 150)
     text("Carefull Someone might see us!", width/2+50, height/2);
     pop();
  }

  if (footprint.y <= 345 && footprint.x > 1050 ){
    footprint.y += speed;
    push();
    textAlign(CENTER);
    textSize(30)
    noStroke();
    fill(105, 46, 14, 150)
    text("Careful someone might see us!", width/2+50, height/2);
    pop();
  }

// Lines for the walls
     stroke(105, 46, 14)
     strokeWeight(1.2);
     line(line_close, line_up, line_far-468, line_up); // upper horisontal line

     line(line_far-371, line_up, line_far, line_up); // rigth upper horisontal line

     line(line_far, line_up, line_far, line_up+35);

     line(line_close, line_up, line_close, 500); //  vertical line

     line(line_close, 500, line_far, 500); // lower horisontal  line

     line(line_far, 345, line_far, 500); //  vertical line

     line(wall_x, line_up, wall_x, 240); // upper room line (requriment)

     line(wall_x,335, wall_x, 500); // lower room line (requriment)

// the outer text
     push();
     noStroke();
     fill(105, 46, 14)
     text(" / The ones that love us never really leave us. You can always find them in here", line_close-1, line_up-15, line_far-490, 20 ); // upper left horisontal line

     text("It does not do to dwell on dreams and forget to", line_far-371, line_up-15, line_far-300, 20) // upper right horisontal line

  push();
     translate(line_far+4, line_up+1)
     rotate(90)
     text("live", 0, 0);
     text("You're just as sane as I", 289, 0) // right vertical line
     pop();

     text("am", line_far-20, 513.5) // lower horisontal line

     text(" / Messers Moony, Wormtail, Padfoot, & Prongs Purveyors of Aids to Magical Mischief-Makers are proud to present THE MARAUDER'S MAP / ", line_close, 513.5); // lower horisontal line

     push();
     translate(line_close-5, 515)
     rotate(-90)
     text("I solemnly swear that I am up to no good / Mischief managed", 0, 0);
     pop();
  pop();

// The items in room of requirement
     push();
     imageMode(CENTER);
     image(banner, 255, 100, 200, 70);
     textAlign(CENTER);
     rectMode(CENTER);
     noStroke();
     fill(105, 46, 14);

     text("The room of requirement", 255, 112, 170, 50)
     textSize(9);
     image(foot_down, 255, 175, 30, 70);
     text("Neville longbottom", 255, 143, 20, 20)
     image(foot, 255, 440, 30, 70);
     text("Luna Lovegood", 255, 471, 20, 20)
     tint(255, 100); // Display at half opacity
     image(spell, 255, spell_y, 20,20);
      if (frameCount % 6 == 0){
        spell_y -= spell_speed
        if (spell_y < 250 || spell_y > 350) {
          spell_speed = spell_speed * -1;
        }
      }
     image(steps, line_far, 220, 250, 250); // steps for the tower
     pop();

// the counting mechanisms
  count++ // the tower footprints
   if (count > 120 && count < 380){
     tower_foot();
     push();
       imageMode(CENTER);
       image(banner1, line_far, 220, 88, 58);
       textAlign(CENTER);
       rectMode(CENTER);
       noStroke();
       textSize(9);
       fill(105, 46, 14);
       text("The north tower", line_far, 223, 40, 40)
     pop();
   } else if (count > 380 ){
     count = 0;
   }

  count_1++ // the stairs
    if (count_1 > 0 && count_1 < 100){
        image(stair, wall_x-1, line_up, 250, 200)
    } else if (count_1 > 180 && count_1 < 280){
        image(stair, wall_x-1, 246, 250, 200)
    } else if (count_1 > 380){
      count_1 = 0;
    }

  count_2++ // the cat paws
    if (count_2 > 100 && count_2 < 132){
        catpaws_left();
    } else if (count_2 > 132  && count_2 < 163){
        catpaws_up();
    } else if (count_2 > 300) {
      count_2 = 0;
      cat_left_x = 400;
      cat_up_y = 300;
    }

  count_3++ // the trap door
    if (count_3 > 0 && count_3 < 95){
      image(trap_door_closed, line_far-110, 410, 90, 70);
    } else if (count_3 > 95 && count_3 < 110){
      image(trap_door_open, line_far-110, 410, 90, 70);
    } else if (count_3 > 110 && count_3 < 300){
      image(trap_door_closed, line_far-110, 410, 90, 70);
    } else if (count_3 > 300) {
      count_3 = 0;
    }

  count_4++ // the footprints to the trap door
    if (count_4 > 40 && count_4 < 65){
      footprint_down();
    } else if (count_4 > 65 && count_4 < 95) {
      footprint_right();
    } else if (count_4 > 95 && count_4 < 103){
      push();
      tint(255, 150);
      imageMode(CENTER);
      image(foot_down, line_far-60, 390, 30, 70);
      pop();
    } else if (count_4 > 300){
      count_4 = 0;
      foot_down_y = 75;
      foot_right_x = 800;
    }
  }
}

function mousePressed(){
  screen = 1;
}

function textbanner(){
  name_data.push(textbox.value())
  change_name += 1
}

function tower_foot(){
  push();
  translate(line_far, 220)
  rotate(-angle);
  fill(0);
  image(foot, 20, 32, 30, 70);
  textSize(9)
  noStroke();
  rectMode(CENTER);
  textAlign(CENTER);
  fill(105, 46, 14);
  text("Severus Snape", 35, 97, 20, 20);
    if (frameCount % 10 == 0){
      angle += 15 // the degress which the ellipse moves for each loop
    }
  pop();
}

function catpaws_left(){
  push();
  imageMode(CENTER);
  tint(255, 150);
  image(cat_left, cat_left_x, 290, 70,40)
  textAlign(CENTER);
  textSize(9)
  noStroke();
  fill(105, 46, 14);
  angleMode(DEGREES);
  translate(cat_left_x-25, 290);
  rotate(90);
  text("Mrs Norris", 0, 0);

  if (frameCount % 4 == 0){
    cat_left_x += 50
  }
  pop();
}

function catpaws_up(){
  push();
  imageMode(CENTER);
  tint(255, 150);
  image(cat_up, 775, cat_up_y, 42, 70);
  textAlign(CENTER);
  noStroke();
  textSize(9)
  fill(105, 46, 14);
  text("Mrs Norris", 775, cat_up_y+25);
  if (frameCount % 4 == 0){
      cat_up_y -= 52
    }
  pop();
}

function footprint_down(){
  push();
  tint(255, 150);
  imageMode(CENTER);
  image(foot_down, 784, foot_down_y, 30, 70);
  textSize(9)
  noStroke();
  rectMode(CENTER);
  textAlign(CENTER);
  fill(105, 46, 14);
  text("Hermione Granger", 784, foot_down_y-32, 20, 20);
  if (frameCount % 4 == 0){
    foot_down_y += 50
  }
  pop();
}

function footprint_right(){
  push();
  tint(255, 150);
  imageMode(CENTER);
  image(foot_right, foot_right_x, 380, 70, 30);
  rectMode(CENTER);
  textAlign(CENTER);
  textSize(9)
  noStroke();
  fill(105, 46, 14);
  translate(foot_right_x-40, 375);
  rotate(90);
  text("Hermione Granger", 0, 0, 20, 20);
  if (frameCount % 4 == 0){
    foot_right_x += 50
  }
  pop();
}
