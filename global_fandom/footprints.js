
class Footprint {
  constructor(){
    this.x = 650
    this.y = 400;
    this.w = 30;
    this.h = 70;
  }

  show() {
  push();
  tint(255, 150);
  imageMode(CENTER);
  image(foot, constrain(this.x, line_close+this.w/2, line_far-this.w/2), constrain(this.y, line_up+28, 470), this.w, this.h)
  pop();
  }
}

class Name {
    constructor(){
      this.x = width/2
      this.y = height/2;
      this.w = 140;
      this.h = 110;
    }

show(){
  push();
  textAlign(CENTER);
  noStroke();
  textSize(10);
  fill(105, 46, 14);
  text(name_data[change_name], constrain(footprint.x, 150, 1185), constrain(footprint.y+28, 108, 498 ))
  pop();
  }
}

class Tower {
  constructor(){
    this.x = line_far
    this.y = 220
    this.size = 250;
  }

  show() {
    push();
      strokeWeight(2);
      stroke(105, 46, 14)
      noFill()
      circle(this.x, this.y, this.size)
      stroke(105, 46, 14, 75)
      circle(this.x, this.y, this.size-146)
    pop();
  }
}
