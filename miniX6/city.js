// images
let block0, block1, block2, block3, block4, block5;
let c, f;
// background
let x = -50;
let y = 0;
let change;
let change_min;
let x_limit = 1400;
let y_limit = 800;
let spaceX = 100
let spaceY = 150;
// car
let min_car = 8;
let car = [];
let keyColor = 45;
// fox
let foxSize = {w:80,h:80};
let fox;
let foxPosX;
let foxPosY;
// score or loss
let score = 0;
let lose = 0;



function preload() {
block0 = loadImage('block0.png');
block1 = loadImage('block1.png');
block2 = loadImage('block2.png');
block3 = loadImage('block3.png');
block4 = loadImage('block4.png');
block5 = loadImage('block5.png');
c = loadImage('car.png');
f = loadImage('fox.png');
}

function setup(){
  createCanvas(windowWidth,windowHeight);
  background(150);

  foxPosX = 650;
  foxPosY = 30;
}

function draw(){

change = floor(random(0,5));
// choosing which block to show
  if (change === 0){
    if (x <= x_limit){
    drawblock0();
    }
  } else if (change === 1){
    if (x <= x_limit){
    drawblock1();
    }
  } else if (change === 2){
    if (x <= x_limit){
    drawblock2();
    }
  } else if (change === 3){
    if (x <= x_limit){
    drawblock3();
    }
  } else if (change === 4) {
// choosing between to blocks that show less than the others
change_min = floor(random(0,2));

  if (change_min === 0){
    if (x <= x_limit){
    drawblock4();
    }
  } else if (change_min === 1){
    if (x <= x_limit){
    drawblock5();
    }
  }
}
// statement that says if x is out of limit move to a new line (background)
if (x >= x_limit && y < y_limit){
  x = -50;
  y+= spaceY;
  }

// the road
noStroke();
fill(150);
rect(450,0,400,2000)

// the fox
imageMode(CENTER);
image(f,constrain(foxPosX, 490,810), constrain(foxPosY,30,height-30), foxSize.w, foxSize.h);

// controls the fox
if (keyIsDown(LEFT_ARROW)) {
   foxPosX -= 5;
 }

 if (keyIsDown(RIGHT_ARROW)) {
   foxPosX += 5;
 }


 if (keyIsDown(UP_ARROW)) {
    foxPosY -= 5;
  }

  if (keyIsDown(DOWN_ARROW)) {
    foxPosY += 5;
  }


// functions conected to the obejct (cars)
checkCarNum();
showCar();
checkHit();
displayScore();
checkResult();

}

function checkCarNum(){
if(car.length < min_car){ // if there is less that the minimum amount of cars the program adds more
  car.push(new Car()); //creating a new object
  }
}

function showCar(){
  /* This shows the cars and moves the cars upwards do to the -=.
  This for-loop creates more cars as long as there is less than 8 cars on the screen*/
for (let i = 0; i < car.length; i++){ // Car.length means number of cars which should always be minimum 8
  car[i].show();
  car[i].move();
  }
}


function checkHit(){
  for (let i = 0; i < car.length; i++) {
      let d =
        dist(foxPosY+foxSize.w/2,foxPosX+foxSize.h/2, // calculates the distance bewteen the fox and the car
        car[i].pos.y+55,car[i].pos.x);

  //if it hits add  +1 to lose.
      if (d < foxSize.w *0.5) {
        lose++;
        car.splice(i,1);
  //if it misses add  +1 to score.
} else if (car[i].pos.y < 0) {
        score++;
        car.splice(i,1);
      }
    }
}

function displayScore() {
    fill(keyColor, 160);
    textAlign(CENTER);
    textSize(17);
    text('You have survived '+ score + " cars",650, 530); // the variable score and lose makes it possible to add a point to the counter
    text('You have been hit by ' + lose + " out of 3 cars", 650,550);
    fill(keyColor,255);
    text('PRESS the LEFT key & RIGHT key to avoid the cars', 650,500)
}

function checkResult() {
  if (lose > 2) { // if 3 or cars hits the fox, the game is over
    fill(keyColor, 255);
    textSize(26);
    text("NO MORE LIVES...GAME OVER", 650, 400);
    noLoop();
  }
}

// functions that contains the block used for the background, which changes each time
function drawblock0(){
imageMode(CORNER);
image(block0, x, y, 100,120);
  x+= spaceX // makes the block move

}

function drawblock1(){
imageMode(CORNER);
image(block1, x, y, 100,170);
  x+= spaceX // makes the block move


}

function drawblock2(){
imageMode(CORNER);
image(block2, x, y, 100,120);
  x+= spaceX // makes the block move

}

function drawblock3(){
imageMode(CORNER);
image(block3, x, y, 100,140);
  x+= spaceX // makes the block move
}

function drawblock4(){
imageMode(CORNER);
image(block4, x, y, 100,140);
  x+= spaceX // makes the block move

}

function drawblock5(){
imageMode(CORNER);
image(block5, x, y, 100,140);
  x+= spaceX // makes the block move

}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
