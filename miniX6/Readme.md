Click [here](https://trinerye.gitlab.io/aestheticprogramming/miniX6/index.html) to view my miniX6 program

Click [here](https://gitlab.com/TrineRye/aestheticprogramming/-/blob/main/miniX6/city.js) to view my sketch.js code

Click [here](https://gitlab.com/TrineRye/aestheticprogramming/-/blob/main/miniX6/car.js) to view my car.js code

![](game.mp4)

### **Describe how does/do your game/game objects work?**

The program is a simple game where a fox needs to avoid the cars in order to survive. Seen as the game is simple, so is the goal, which is to stay alive for as long as possible. My original plan was to make a maze-like city that expanded as the player reached the edge of the screen, however, it proved to be too difficult for me. 

There are three main elements to this game 1) the background, 2) the cars, and 3) the fox, however, only the cars are object-oriented. I began with the background as I wished to make a game where the scenery changed each round. The background consists of six city block images that moves across the screen do to `x+=spaceX` and creates a new line with `y+=spaceY` if the blocks reach the edge. This structure is based on several if-else statements that controls what to display, so for each framecount the block will move and change to a different cityscape randomly. However, I wanted the lake and the playground to appear less frequently than the rest, so I used random within random to decrease the odds of those blocks showing. 

Since the cars are an object, I created a second file to hold the class: Car. The attributes, which I chose to install in each object are the variables: speed, position, and size. The starting point of each car is chosen at random and so is the speed, however both elements have a limit they must abide by. Also, a minimum number of cars must always be displayed on the screen, which is ensured through the use of a `for-loop` that checks if the length of the car array is less than the minimum amount, if so, more cars will be produced. The cars will also disappear if out of screen or hitting the fox, thereby creating same response as before, producing new cars. 

The fox is not an object as I had some difficulties with understanding how classes and objects work, and since Winnie’s Tofu example uses a function-based Pacman, I did the same with my fox. What makes it move is the conditional statements involving the arrow keys, however, the fox too has I limit as it can not move outside the road, otherwise it will ruin the background as the fox create a “tale” of itself disrupting the cityscape blocks. This is obtained through the use of `constrain`.


### **Describe how you program the objects and their related attributes, and the methods in your game.**

For this program, I have created the class Car that acts as a template from which each object is created. The class contains different element that all contribute to what it means to be this object such as properties/attributes and behaviors/methods. 

The requirement for this week’s miniX was to make at least one object, so I chose the cars as my object as it seemed practical for them to have the same attributes and methods. The function `constructor()` initializes the object with the attributes below, which involves variables such as properties of speed, position, size, rotating etc. What sets object-oriented programming apart from the programming we have done so far, is the keyword `this.` which refers to the current object that is displayed within the program thereby allow variations of the object within the program. 

The term method is used to describe an object’s behaviors which can be applied to each object instance. However, it is possible for each object to move differently from one another such as variation of speed, position etc. if programmed to do so. To display the cars on the screen I had to use the method `show()` since without it, the object will be created in the background and not show on the screen. A second function `move()` was needed to make the cars move from the bottom of the screen to the top. 

### **The characteristics of object-oriented programming and the wider implications of abstraction**

Abstraction is a key characteristic of Object-Oriented Programming (OOP) and can be described as a process in which physical objects get translated into computational objects “and in doing so, certain details and contextual information are inevitably left out (Soon & Cox, 2020).” 
Abstraction makes it possible to understand the complexity of objects by removing certain details and instead presenting a concrete model. According to Beatrice Fazi and Matthew Fuller, this relationship between concrete and abstracted computation continues to shape the world in that “computation not only abstracts from the world in order to model and represent it […] it also partakes in it (ibid.).” 
The objects within OOP do not just concern themselves with representation or the functions and logic that compose the objects but do (or should) consider the wider relations and interactions between and with the computational, and thus attend to how computational objects afford and shape certain behaviors and actions. This is important, especially when learning to program one “engage in the politics of this movement between abstract and concrete reality which is never a neutral process (ibid.).” We must acknowledge that “objects are designed with certain assumptions, biases, and worldviews (ibid.)” so that we can prevent unethical abstractions and instead create or redefine objects in a way that represents us all. 

### **A wider cultural context**

My intention was to make a simplified version of the game Crossy Road (2014) where one has to continuously cross the roads ahead, but I quickly realized that this game structure was too advanced for me, so I altered the game and instead created a new one that resembles the old arcade game Monaco GP (1979) in terms of level structure. 
Since the game is quite simple and resembles other well-established games, I didn’t spend much time creating instructions on how to play the game, as I figured future players would be familiar to such games. However, as I answered the question above, I realized that my assumptions about the players’ knowledge could be considered biased. Not everybody grew up with games or had an interest in playing them, so even though my game is simple and has some instructions, it might not be familiar to everyone. 


### **References**

Winnie Soon & Geoff Cox; 'Aesthetic Programming: A Handbook of Software Studies', 2020

https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/class07

https://www.crossyroad.com/crossy-road-classic-press-kit 

https://codeheir.com/2019/03/31/how-to-code-monaco-gp-1979-8/?fbclid=IwAR2dHeZGt5pQiHyOh7_r67Vuj7GRHKlbrMTPPl0viKtOY9dTDhZ1OUEI00A 

**Images**

https://www.vectorstock.com/royalty-free-vector/red-cartoon-muscle-car-top-view-vector-21974587

https://www.vectorstock.com/royalty-free-vector/cute-cartoon-fox-in-modern-simple-flat-style-vector-20261488

