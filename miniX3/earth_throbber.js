
let img;
let angle = 0;
let x = 20;
let bar_length = 250;
let stars;

function preload() {
img = loadImage('earth_2.png'); // uploades the image
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  createCanvas(windowWidth, windowHeight);
  stars = createGraphics(windowWidth, windowHeight); // creates a p5.Render obejct that can be drawn untop of the background while remaining static
    for(let i = 0; i < 200; i++) { // the for-loop creates multiple stars
      stars.noStroke();
      stars.fill(255,80);
      stars.ellipse(random(width),random(height),random(2,4),random(2,4)) // the stars spawn randomly across the screen
    }

  angleMode(DEGREES);
  frameRate(50);
}

function draw() {

  background(19, 22, 53);
  image(stars,0, 0);   // draws the stars from setup

  push();
  imageMode(CENTER);
  image(img, windowWidth/2, windowHeight , 420, 420); // image earth
  pop();

  insideloadingbar();

  push();
  noFill();
  strokeWeight(2)
  stroke(255);
  rectMode(CENTER);
  rect(width/2,340,bar_length,30,20); // the loadidng bar
  pop();

  if (mouseIsPressed == true) { //change the sun to a moon if mouseIsPressed
    push();
    strokeWeight(2);
    stroke(255);
    textSize(35);
    fill(100);
    textAlign(CENTER);
    text('sleeping...',width/2, 300);
    pop();

    push();
    translate(windowWidth/2, windowHeight) // makes a new starting point
    rotate(angle); // rotates the obejct around the starting point
    noStroke();
    fill(250);  // moon
    ellipse(350, 350, 60,60);
    ellipse(-350, -350, 60,60);
    fill(230);  // moon holes
    ellipse(340, 355, 20,17);
    ellipse(-340, -355, 20,17);
    ellipse(365, 345, 18,23);
    ellipse(-365, -345, 18,23);
    ellipse(345, 325, 15,9);
    ellipse(-345, -325, 15,9);
    ellipse(360, 370, 9,8);
    ellipse(-360, -370, 9,8);
    ellipse(335, 345, 10,8);
    ellipse(-335, -345, 10,8);
    angle = angle + 0.5  // the degress which the ellipse moves for each loop
    pop();

    noStroke(); // blue alpha arc on top of the image
    fill(0,0,20,170);
    arc(windowWidth/2, windowHeight, 398, 401 , 180, 0, CHORD);

    noLoop();

  } else {
    push();
    strokeWeight(2);
    stroke(255);
    textSize(35);
    fill(random(100,200),random(100,200),random(100,200));
    textAlign(CENTER);
    text('loading...', width/2, 300);
    pop();

    push();
    translate(windowWidth/2, windowHeight) // makes a new starting point
    rotate(angle)   // rotates the obejcts around the starting point
    fill(255,215,0); // sun
    ellipse(350, 350, 75,75);
    ellipse(-350, -350, 75,75);
    fill(255,215,0,100); // inner glow
    ellipse(350, 350, 90,90);
    ellipse(-350, -350, 90,90);
    fill(255,215,0,50); // outer glow
    ellipse(350, 350, 100,100);
    ellipse(-350, -350, 100,100);
    angle = angle + 0.5 // the degress which the ellipse moves for each loop. IT DOES NOT STOP AT 360 BUT CONTINUES FOREVER
    pop();
    }
  }

function insideloadingbar(){
  push();
  rectMode(LEFT,TOP);
  noStroke();
  fill(random(100,200),random(100,200),random(100,200));
  rect(width/2-bar_length/2,325,x,30,20); // rect inside loading bar
  x+=0.4

  if (x > bar_length) { // beginning and end condition for the rect inside the loading bar
    x = 20;
  }
  pop();
}


function mouseReleased() { // return loop when mouse is released
  loop();
}
