## MiniX3

Click [here](https://trinerye.gitlab.io/aestheticprogramming/miniX3/index.html) to view my miniX3 program

Click [here](https://gitlab.com/TrineRye/aestheticprogramming/-/blob/main/miniX3/earth_throbber.js) to view my code

### **What do you want to explore and/or express?**

<img src="miniX3/earth_throbber_sketch.png">

![](earth_throbber.mp4)

This week, we had to design a throbber that illustrates the ongoing process within a system, so when I read Hans Lammerant chapter “How humans and machines negotiate experience of time” I found the idea of “renegotiating time with your computer” by setting the system to sundial time to be quite remarkable, as it challenges our views on both time and processing. With this in mind, I began to sketch and develop an idea of what a sundial time experience within a system might look like (Lammerant, 2018). With this throbber, I explored a new set of commands such as `rotate`, `angleMode`, `loop` and `noLoop` which allowed me to express the “day-night time cycle” mentioned by Lammerant (Lammerant, 2018). 

### **What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?**
The main time-related function I have used to create this throbber is the _rotate_ function, which creates a never-ending loop resembling the sun and moon cycle seen from an earth perspective. The rotate function will make the object rotate around an origin, but the element that gives the function its time-related abilities is the _draw_ function combined with either _radians_ or _angle_. Without these two functions, the object will face a new direction but remain static. The draw function is in itself a time-related function since it re-runs the code 60 times per second or whatever the framerate might be and thereby creates a loop. However, without the _angleMode_ function and angle the object would still be stuck in one place. When angleMode is set to _DEGREES_ and rotate is set to _rotate(angle)_, one has to add a value to the angle in order for it to move. If these requirements are met, the object will move in a circular motion around the predetermined origin. 
What makes the throbber sundial dependent are the functions _loop, noLoop, mouseIsPressed, mousePressed_ and _mouseReleased_. The graphical elements that change shape, color or text are all placed within the function mouseIsPressed, so when the mouse is pressed the time changes from day to night as the sun is replaced by the moon. Also, the text changes from loading to sleeping, which I have done so to illustrate Lammerate’s notion on how night means “_out of time_” (Lammerant, 2018). To support this, I used a noLoop/mousePressed and loop/mouseReleased combination to “stop time” which simultaneously freezes the loading bar as to show the “night state” that controls the system until the sun rises again.

### **Think about a throbber that you have encounted in digital culture**

Throbbers are often used to inform the user of the computer’s inner state but not explicitly, instead what these waiting icons do is indicate the time it takes to perform the tasks necessary to connect the computer to different  servers. The throbber’s purpose is to show the user some sign of progress while the system works, however, some throbbers do more than just show the passing of time, some also entertain. The green Sims diamond is such a throbber as it also displays text and cheeky sims remarks from the game to entertain the user while the game is loading. This gives the user the opportunity to see the processing time as something more than time wasted but time to explore and investigate this elaborate world which Sims is.


### **References**

Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018)

https://p5js.org/reference/#/p5/rotate

https://p5js.org/reference/#/p5/angleMode

https://p5js.org/reference/#/p5/noLoop

https://p5js.org/reference/#/p5/mouseIsPressed

https://p5js.org/reference/#/p5/loadImage


**Image reference**

https://cdn.imgbin.com/2/10/20/imgbin-globe-emojipedia-earth-world-earth-D3BnAx2R4KShexW80VWZRfd58.jpg 
