## MiniX1

[_Click to view my **miniX1**_](https://trinerye.gitlab.io/aestheticprogramming/miniX1/index.html)

[_Click to view my **code**_](https://gitlab.com/TrineRye/aestheticprogramming/-/blob/main/miniX1/snow_globe.js)


### **What have you produced?**

Before this week’s MiniX I had no experience with coding, so when Winnie first asked us to make a circle, I was concerned about my abilities to write a functioning code. But I overcame that challenge which sparked something in me that motivated me to improve upon what I had just learned. I still wanted to play with circles in both static and dynamic forms, however, I also wanted to challenge myself once again by adding an interactive element. 

The result, a snow globe that changes color when pressed. 

![](snow_globe.mp4)

The video above showcases a simple interaction with the snow globe, which changes color whenever the mouse is pressed. 

I used the `let` function before creating my `setup()`, which allowed me to declare a set of variables (r, g, and b) that I would be using later on. Under the setup I created a canvas and do to it’s relevant to the looping-snow effect I defined the `background()` and the `frameRate()` here as well. The framerate default is 60 frames per second, but I changed it to 3 to make each “snow flake” more visible. The variables that I had created symbolized the RGB color codes, so I set each variable to equal `random(255)`, which means that each color code can choose a random value between 0 and 255. Then I created the function `draw()` and started building the snow globe. Each element is made with a geometric shape such a the `ellipse()`, the `rect()`, and the `triangle()`. The ellipse that makes up the globe is important because the functions tied to it, is what makes the globe change color. The `fill()` function is set to (r, g, b, 127) so when the mouse is pressed the ellipse will turn a random color while also accounting for the background color set at 127. For aesthetic purposes I set the `strokeWeight()` to 2 to make the line thicker and then set the color of the `stroke()` to (r, g, b). I then made the snowman with some of the shapes mentioned above, which tested my skills in how to read coordinates. Next, used the `for()` function which gives one the opportunity to create loops and set the coordinates of the ellipse that makes up the snowflakes to random within a certain area so that the snow would spawn randomly inside the globe. The last element within the function draw was the snow layer and wooden base, which had to be typed last so that the shapes would cover some of the globe to make it seem as though the shapes were connected. To make the globe change color I used a function called `mousePressed()`, but in order to make it work I had to use the `let` function once again to define that if the mouse is pressed within the diameter less than 250 from the point (350, 200), which is the middle of the color changing circle, then the circle will turn a random color. For this I also needed the `dist()` and `if()` functions. 


### **How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**

Challenging but worth it. I have been looking forward to this course ever since I started at Aarhus University, so to finally learn how to program and write my own code is something that I am extremely excited about. What I have learned so for is that SYNTAX IS EVERYTHING! In my search on how to do certain things I looked at other people’s code and tried to implement it in my own, but it was to no surprise quite difficult. However, after hours of trails and errors I managed to come up with something that I am pleased with. So, my experience with coding so far have been a success.  


### **How is the coding process different from, or similar to, reading and writing text?**

When I write, I often use a word-processing program such as Microsoft Word or Google Docs, which is helpful incase of a spelling mistake. A code editor on the other hard doesn’t tell when I spelled something wrong, instead it shows me a black page when I try to run the code, and let me tell you all, I have seen that black page a lot of times these last few days. However, as a worked on my code and learned by reading other people’s work I came to realize how the language of Javascript is tied together. I am in no way an expert, after all, it has only been I week since I started, but I am now aware of how specific one has to be in order to make an executable code.  


### **What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?**

Throughout this week my skills on how to write code have developed immensely, and especially the chapter on “Getting Started” by Winnie Soon combined with the introduction video to p5.js my Lauren McCarty has helped me to understand the possibilities of programming and inspire me to explore this new world.

### **References**

https://p5js.org/reference/#/p5/let 
 
https://p5js.org/reference/#/p5/frameRate 

https://p5js.org/reference/#/p5/random 

https://p5js.org/reference/#/p5/fill 

https://p5js.org/reference/#/p5/strokeWeight 

https://p5js.org/reference/#/p5/stroke 

https://p5js.org/reference/#/p5/for 

https://p5js.org/reference/#/p5.Element/mousePressed 

https://p5js.org/examples/hello-p5-interactivity-1.html 

https://p5js.org/reference/#/p5/dist 

https://p5js.org/reference/#/p5/if-else 

Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48

Video: Lauren McCarthy, Learning While making P5.JS, OPENVIS Conference (2015).
