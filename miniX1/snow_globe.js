// for red, green, and blue color values
let r, g, b;

function setup() {
  createCanvas(500, 500);
  background(200);
  frameRate(3); // how quick the frame refresh
  // Pick colors randomly
  r = random(255);
  g = random(255);
  b = random(255);



}

function draw() {

  // Draw the circle
  strokeWeight(2);
  stroke(r, g, b);
  fill(r, g, b, 127);
  ellipse(250, 250, 250, 250);

  noStroke();
  fill(255);
  ellipse(250,270,60,60); //Snowman body
  ellipse(250,225,40,40); //Snowman head

  fill(0);
  ellipse(243,220,4,4) // left eye
  ellipse(257,220,4,4) // Right eye

  noStroke();
  fill(255,132,0);
  triangle(250, 235, 246, 225, 254, 225); // Nose


  for (let i = 0; i < 60; i++) { // Creates a loop
  noStroke();
  fill(243, 243, 243);
  ellipse(random(161,340),random(161,340),4,4); // makes the circle spawn at random coordinate
  }

  fill(245);
  rect(133, 285, 234, 87); // the snow-base

  fill(153, 100, 45);
  rect(130, 290, 239, 87); // the wood-base

  triangle(100, 377, 130, 290, 254, 377); // left side of base

  triangle(250, 377, 370, 290, 400, 377); // left side of base


}

// When the user clicks the mouse
function mousePressed() {
  // Check if mouse is inside the circle
  let d = dist(mouseX, mouseY, 250, 250);
  if (d < 125) {
    // Pick new random color values
    r = random(255);
    g = random(255);
    b = random(255);
  }
}
